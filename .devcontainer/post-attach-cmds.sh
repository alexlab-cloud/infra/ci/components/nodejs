#!/bin/bash

# Extension script containing shell commands that are run once attached to the dev environment container.

# shellcheck source=/dev/null
source /home/vscode/.bashrc
